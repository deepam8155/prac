import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB as gnb
from sklearn.metrics import accuracy_score
data = pd.read_csv('pima-indians-diabetes.csv')
print data.describe()
features = ['Pregnancies', 'Glucose', 'BloodPressure', 'SkinThickness', 'BMI', 'Age', 'Insulin', 'DiabetesPedigreeFunction']
target = 'Class'
train, test = train_test_split(data, test_size=0.2)
clf = gnb().fit(train[features], train[target]) 
y_predicted = clf.predict(test[features])
print "Accuracy ",round(accuracy_score(test[target], y_predicted)*100,2)," %"