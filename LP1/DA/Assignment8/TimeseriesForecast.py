
import numpy as np
from sklearn import linear_model
import matplotlib.pyplot as plt
import pandas as pd

def show_plot(FlightDate,passengers):
	linear_mod = linear_model.LinearRegression()
	FlightDate = np.reshape(FlightDate,(len(FlightDate),1)) # converting to matrix of n X 1
	passengers = np.reshape(passengers,(len(passengers),1))
	linear_mod.fit(FlightDate,passengers) #fitting the data points in the model
	plt.scatter(FlightDate,passengers,color='yellow') #plotting the initial datapoints 
	plt.plot(FlightDate,linear_mod.predict(FlightDate),color='blue',linewidth=3) #plotting the line made by linear regression
	plt.show()
	return

def predicted_passengers(FlightDate,passengers,x):
	linear_mod = linear_model.LinearRegression() #defining the linear regression model
	FlightDate = np.reshape(FlightDate,(len(FlightDate),1)) # converting to matrix of n X 1
	passengers = np.reshape(passengers,(len(passengers),1))
	linear_mod.fit(FlightDate,passengers) #fitting the data points in the model
	predicted_passengers =linear_mod.predict([[x]])
	return predicted_passengers[0][0],linear_mod.coef_[0][0] ,linear_mod.intercept_[0]

df = pd.read_csv('airlines.csv')
FlightDate = df['FlightDate'].tolist()
passengers = df['Passengers'].tolist()


show_plot(FlightDate,passengers) 
#image of the plot will be generated. Save it if you want and then Close it to continue the execution of the below code.

predicted_passengers, coefficient, constant = predicted_passengers(FlightDate,passengers,28)  
print("The Number of passengers for 28th Feb will be: $",int(predicted_passengers))
print("The regression coefficient is ",str(coefficient),", and the constant is ", str(constant))
print("the relationship equation between FlightDate and passengers is: price = ",str(coefficient),"* date + ",str(constant))
