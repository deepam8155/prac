class Node():
    def __init__(self, parent=None):
        self.parent = 0
        #self.position = position

        self.g = 0
        self.h = 0
        self.f = 0
        self.id = 0

    # def __eq__(self, other):
    #     return self.id == other.id

def astar(maze,start,end,H):

    #creating start and end ndoes
    start_node = Node(start)
    start_node.g = start_node.h = start_node.f = start_node.id = 0
    end_node = Node(end)
    end_node.g = end_node.h = end_node.f = end_node.id = end

    #Initialize the open and closed lists
    open_list = []
    closed_list = []
    
    #add start node
    open_list.append(start_node)

    #loop until end reached
    while(len(open_list)>0):

        #Get Current node
        current_node = open_list[0]
        current_index = 0

        for index, item in enumerate(open_list):
            if item.f < current_node.f:
                # print(item.id)
                current_node = item
                current_index = index


        # Pop current off open list, add to closed list
        open_list.pop(current_index)
        closed_list.append(current_node)

        # Found the goal
        if current_node.id == end_node.id:
            path = []
            current = current_node
            while current.parent != 0:
                path.append(current.id)
                current = current.parent
            return path[::-1] # Return reversed path

        # Generate Children
        children = []
        for index,i in enumerate(maze[current_node.id]):
            if i != 0:
                new_node = Node(0)
                new_node.id = index
                new_node.h = H[index]
                new_node.parent = current_node
                children.append(new_node)

        #loop through Children
        for child in children:

            #Child is on closed list check
            for closed_child in closed_list:
                if child == closed_child:
                    continue

            #create f,g,h values
            child.g = current_node.g + maze[current_node.id][child.id]
            child.f = child.g + child.h


            # Child is already in the open list
            for open_node in open_list:
                if child == open_node and child.g > open_node.g:
                    continue

            # Add the child to the open list
            open_list.append(child)

    return None
        




def main():

    maze=[[0,1,3,0,0,0],
    [0,0,5,8,0,0],
    [0,0,0,4,2,0],
    [0,0,0,0,0,3],
    [0,0,0,0,0,1],
    [0,0,0,0,0,0]]

    #print(A)

    H=[0,4,3,2,1,0]

    start = 0
    end = 5

    path = astar(maze, start, end,H)

    if path is not None:
        print("Target Found at path: ")
        print(path)
    else:
        print("Cannot find the target")


if __name__ == '__main__':
    main()


